
figure(1); clf;
hold on;

for i = 1:numel(schnitzcells)
    
    plot([schnitzcells(i).time],[schnitzcells(i).length_fitNew])
    
end

% Fields of interest
schnitzcells.frame_nrs % framenrs it lived in
schnitzcells.time % timepoints it lived
schnitzcells.length_fitNew % length
schnitzcells.muP5_fitNew % growthrate 

schnitzcells.G6_mean % GFP concentration
schnitzcells.time_atG % Corresponding points in time

schnitzcells.dG5_cycCor % GFP productoin (absolute)
schnitzcells.time_atdG % corresponding points in time